#include <stdio.h>
#include <time.h>
#include <windows.h>
#include <wtsapi32.h>
#include <shlwapi.h>

#define WM_WTSSESSION_CHANGE 0x2B1

#define WTS_SESSION_LOCK 0x7
#define WTS_SESSION_UNLOCK 0x8

#define ID_TRAY_APP_ICON                     6000
#define ID_TRAY_EXIT_CONTEXT_MENU_ITEM       7000
#define ID_TRAY_TITLE_CONTEXT_MENU_ITEM      7001
#define ID_TRAY_SEPARATOR_CONTEXT_MENU_ITEM  7002
#define WM_TRAYICON ( WM_USER + 1 )

#define MB_FLAGS MB_OK | MB_ICONWARNING | MB_SETFOREGROUND | MB_SERVICE_NOTIFICATION | MB_TOPMOST

static unsigned int TaskbarCreated;
static HWND MainWnd;
static HMENU MainMenu;
static NOTIFYICONDATA NotifyIconData;
static TCHAR Title[]=TEXT("WTS Event Monitor");

static char* LockTitleDefault="Attenzione";
static char* LockMessageDefault="Disabilitare il telefono!";
static char* UnlockTitleDefault="Attenzione";
static char* UnlockMessageDefault="Riabilitare il telefono!";
static int StartLunchHourDefault=12;
static int StartLunchMinDefault=0;
static int EndLunchHourDefault=14;
static int EndLunchMinDefault=0;

static char LockTitle[200];
static char LockMessage[200];
static char UnlockTitle[200];
static char UnlockMessage[200];
static int StartLunchHour;
static int StartLunchMin;
static int EndLunchHour;
static int EndLunchMin;

static void LoadDefaults()
{
  strcpy(LockTitle, LockTitleDefault);
  strcpy(LockMessage, LockMessageDefault);
  strcpy(UnlockTitle, UnlockTitleDefault);
  strcpy(UnlockMessage, UnlockMessageDefault);
  StartLunchHour=StartLunchHourDefault;
  StartLunchMin=StartLunchMinDefault;
  EndLunchHour=EndLunchHourDefault;
  EndLunchMin=EndLunchMinDefault;
}

static void OnSession(WPARAM wParam)
{
  time_t t;
  struct tm* ts;

  time(&t);
  ts=localtime(&t);

  if((60*ts->tm_hour+ts->tm_min < 60*StartLunchHour+StartLunchMin) || (60*ts->tm_hour+ts->tm_min > 60*EndLunchHour+EndLunchMin))
  {
    switch(wParam)
    {
      case WTS_SESSION_LOCK:
        MessageBox(NULL, TEXT(LockMessage), TEXT(LockTitle), MB_FLAGS);
        break; 

      case WTS_SESSION_UNLOCK:
        MessageBox(NULL, TEXT(UnlockMessage), TEXT(UnlockTitle), MB_FLAGS);
        break; 
    }
  }
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
    case WM_CREATE:
      MainMenu = CreatePopupMenu();
      AppendMenu(MainMenu, MF_STRING | MF_GRAYED, ID_TRAY_TITLE_CONTEXT_MENU_ITEM,  TEXT( Title ) );
      AppendMenu(MainMenu, MF_SEPARATOR, 0x0, NULL );
      AppendMenu(MainMenu, MF_STRING, ID_TRAY_EXIT_CONTEXT_MENU_ITEM,  TEXT( "Exit" ) );
      break;
    
    case WM_TRAYICON:
      {
        switch(wParam)
        {
          case ID_TRAY_APP_ICON:
            break;
        }

        if (lParam == WM_RBUTTONDOWN)
        {
          POINT curPoint;
          UINT clicked;

          GetCursorPos( &curPoint ) ;
          SetForegroundWindow(hWnd); 
          
          clicked = TrackPopupMenu( MainMenu, TPM_RETURNCMD | TPM_NONOTIFY, curPoint.x, curPoint.y, 0, hWnd, NULL);

          //SendMessage(hwnd, WM_NULL, 0, 0); // send benign message to window to make sure the menu goes away.
          if (clicked == ID_TRAY_EXIT_CONTEXT_MENU_ITEM)
          {
            // quit the application.
            PostQuitMessage(0) ;
          }
        }
      }
      break;


    case WM_DESTROY:
      WTSUnRegisterSessionNotification(hWnd);
      PostQuitMessage(0);
      break;

    case WM_WTSSESSION_CHANGE:
      OnSession(wParam);
      break; 
    default:
      return DefWindowProc(hWnd, message, wParam, lParam);
  }

  return 0;
} 

static BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
  WNDCLASSEX wc={ 0 };
  TCHAR szClassName[]=TEXT("WTSMonitor");
  int ok=1;

  // I want to be notified when windows explorer
  // crashes and re-launches the taskbar.  the WM_TASKBARCREATED
  // event will be sent to my WndProc() AUTOMATICALLY whenever
  // explorer.exe starts up and fires up the taskbar again.
  // So its great, because now, even if explorer crashes,
  // I have a way to re-add my system tray icon in case
  // the app is already in the "minimized" (hidden) state.
  // if we did not do this an explorer crashed, the application
  // would remain inaccessible!!
  TaskbarCreated = RegisterWindowMessage("TaskbarCreated") ;

  wc.hInstance=hInstance;
  wc.lpszClassName=szClassName;
  wc.lpfnWndProc=WndProc;
  wc.cbSize = sizeof (WNDCLASSEX);

  wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
  wc.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
  wc.hCursor = LoadCursor (NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH)COLOR_APPWORKSPACE ;

  if(!RegisterClassEx(&wc))
    ok=0;

  if(ok)
  {
    MainWnd=CreateWindowEx(0, szClassName, Title,
      WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
      0, 0, NULL, NULL, hInstance, NULL
    );
    if(!MainWnd)
      ok=0;
  }

  if(ok)
  {
    if(!UpdateWindow(MainWnd))
      ok=0;
  }

  if(ok)
  {
    if(!WTSRegisterSessionNotification(MainWnd, 0))
      ok=0;
  }

  return (ok ? TRUE: FALSE);
}

// Initialize the NOTIFYICONDATA structure.
// See MSDN docs http://msdn.microsoft.com/en-us/library/bb773352(VS.85).aspx
// for details on the NOTIFYICONDATA structure.
static void InitNotifyIconData(HINSTANCE hInstance)
{
  memset( &NotifyIconData, 0, sizeof( NOTIFYICONDATA ) ) ;
  NotifyIconData.cbSize = sizeof(NOTIFYICONDATA);
  
  // Tie the NOTIFYICONDATA struct to our
  // global HWND (that will have been initialized
  // before calling this function)
  NotifyIconData.hWnd = MainWnd;
  // Now GIVE the NOTIFYICON.. the thing that
  // will sit in the system tray, an ID.
  NotifyIconData.uID = ID_TRAY_APP_ICON;
  // The COMBINATION of HWND and uID form
  // a UNIQUE identifier for EACH ITEM in the
  // system tray.  Windows knows which application
  // each icon in the system tray belongs to
  // by the HWND parameter.
  
  NotifyIconData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
  NotifyIconData.uCallbackMessage = WM_TRAYICON;
  NotifyIconData.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(100)) ;
  strcpy(NotifyIconData.szTip, Title);
}

static int GetValue(char* str, char* name, char* value)
{
  char* p;
  int ok=1;

  p=strchr(str, '=');
  if(p)
  {
    strncpy(name, str, p-str);
    name[p-str]='\0';
    strcpy(value, p+1);

    p=value+strlen(value)-1;
    if(*p=='\n')
    {
      *p=0;
      if(*(--p)=='\r')
        *p=0;
    }
  }
  else
    ok=0;

  return ok;
}

static int LoadConfig()
{
  int ok=1;
  FILE* fp;
  TCHAR buffer[MAX_PATH]={0};
  TCHAR * out;
  DWORD bufSize=sizeof(buffer)/sizeof(*buffer);
  char inifile[MAX_PATH];
  char name[50+1];
  char value[200+1];
  char line[250+1];

  // Get the fully-qualified path of the executable
  if(GetModuleFileName(NULL, buffer, bufSize)==bufSize)
  {
      // the buffer is too small, handle the error somehow
  }

  // Go to the beginning of the file name
  //out = PathFindFileName(buffer);
  out=buffer;

  // Set the dot before the extension to 0 (terminate the string there)
  *(PathFindExtension(out)) = 0;
  sprintf(inifile, "%s.ini", out);

  fp=fopen(inifile, "r");
  if(!fp)
    ok=0;

  if(ok)
  {
    while(!feof(fp))
    {
      fgets(line, sizeof(line), fp);
      if(GetValue(line, name, value))
      {
        if(StrCmpI(name, "LockTitle")==0)
          strcpy(LockTitle, value);
        else if(StrCmpI(name, "LockMessage")==0)
          strcpy(LockMessage, value);
        else if(StrCmpI(name, "UnlockTitle")==0)
          strcpy(UnlockTitle, value);
        else if(StrCmpI(name, "UnlockMessage")==0)
          strcpy(UnlockMessage, value);
        else if(StrCmpI(name, "StartLunchHour")==0)
          StartLunchHour=atoi(value);
        else if(StrCmpI(name, "StartLunchMin")==0)
          StartLunchMin=atoi(value);
        else if(StrCmpI(name, "EndLunchHour")==0)
          EndLunchHour=atoi(value);
        else if(StrCmpI(name, "EndLunchMin")==0)
          EndLunchMin=atoi(value);
      }
    }
  }

  if(fp)
    fclose(fp);
  
  return ok;
}

int wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR lpCmdLine, int nCmdShow)
{
  MSG msg;
  BOOL bRet;
  CHAR* lpBuffer;

  HeapSetInformation(NULL, HeapEnableTerminationOnCorruption, NULL, 0);

//  ShowWindow(MainWnd, SW_HIDE);

  LoadDefaults();
  LoadConfig();

  // Perform application initialization.
  if (!InitInstance(hInstance, nCmdShow))
  {

    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), 0, lpBuffer, 100, NULL);
    MessageBox(NULL, lpBuffer, "Errore!", MB_FLAGS);
    LocalFree(lpBuffer);

    return FALSE;
  }

  InitNotifyIconData(hInstance);
  Shell_NotifyIcon(NIM_ADD, &NotifyIconData);

  ZeroMemory(&msg, sizeof(msg));

  while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
  { 
    if (bRet == -1)
    {
      fprintf(stderr, "Error!");
      return FALSE;
      // handle the error and possibly exit
    }
    else
    {
      TranslateMessage(&msg); 
      DispatchMessage(&msg); 
    }
  }

  Shell_NotifyIcon(NIM_DELETE, &NotifyIconData);

  //return msg.wParam;
  return 0;
}
